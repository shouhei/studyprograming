#PHP基礎文法最速マスター 2013/11/19

基本的なところを一気に抑えておくためのメモとして。
既存のものより情報量少なめに、文法に重点を置いた。
必要があれば後々加筆修正する。


## PHPとは
「Php Hypertext Processing」 を縮めて「PHP」。

Hypertext Processing とか言っているけれど、普通にプログラミング言語として使えます。

他の言語との比較の際、一番の特徴として挙げられるのが、HTMLとの共存の仕組みが備わっていること。

2013/11/16 現在PHPの最新バージョンが、5.5系列

（OS X　mavericksの標準で入っているのが、5.4系列)

PHPについて、わからないことがあれば[PHP.net](http://www.php.net/)で検索すると大概見つかる。

（わかりやすいかどうかは、置いておいて。）


## 前提知識
PHPのファイルを作成するときの拡張子として最も一般的なものは「*.php」という形

PHPを記述しはじめるときは「**<?php**」で始めるのが基本。

ファイルの最後にPHP以外のもの、つまりはHTMLを記述したい時だけ、「**?>**」を記述して、HTMLタグを書き始めれば良い。

逆に言うと、**ファイルがPHPで終了するときは「?>」を記述しない。**

これは、理由が存在する（ここで説明するには少々長くなる)ので、守るべきことである。

	例

	<?php

		echo "Hello";

	?>

	<p>こんにちは</p>

	<?php

		echo "GoodNight";


##実行方法 
コマンドラインからは

	C:Users\hogehoge>php hogehoge.php

コマンドラインから文法チェックをしたいときは

	C:Users\hogehoge>php -l hogehoge.php

コマンドラインからサーバーを起動し、実行したい場合は

	C:Users\hogehoge>php -S localhost:8000 -t /path/to/your/scriptDirectory

以上の時

	php -*

の「-*」は、コマンドラインからPHPを起動するためのオプションです。

詳しくは[PHPのマニュアル](http://www.php.net/manual/ja/features.commandline.options.php)を見てください。


## コメントの書き方

これもいくつかあります


	<?php

	//コメントの書き方1

	#コメントの書き方2

	/*

 	 *もし複数行にわたってコメントを記述したい場合は

	 *以下のように記述します。

	 */


## 宣言と型
変数の宣言は「**$**」を利用して、以下のようにして行える。

どのような型を使うかは宣言せずに、

最初に代入された際、自動的に型が決まる。

型には「 **文字列**」「 **整数**」「 **浮動小数点**」「 **配列**」「 **論理**」「 **オブジェクト**」等がある。

	<?php

		$hoge = "hogehoge"; //文字列

		$num = 1234; //整数

		$ary = array(); //配列 



## 出力
出力に関してはいくつかある

	<?php
	//純粋な出力、下2つの違いは気になれば各自で。

	echo "HelloWorld";

	print $hoge;

	//デバッグ用出力、ちなみにvar_dumpのほうが詳しい。

	print_r($foo);

	var_dump($bar);


## 文字
文字関しては次のように利用する。

	<?php
		echo "hogehoge";

		$fizz = "buzz";

		$foo = 'bar';

文字列の連結方法は
	
	<?php
		echo "hello" .'world';
		//結果はhelloworld

		$foo = "fizz";
		$bar = 'buzz';

		echo $foo . $bar;
		//結果はfizzbuzz

PHPで文字を出力する際の注意

	<?php
		$fizz = "buzz";

		echo "hogehoge" . $fizz;
		//結果は　hogehogebuzz

		echo 'hogehoge' . '$fizz';
		//結果は hogehoge$fizz

		echo 'hogehoge' . "$fizz";
		//結果は　hogehogebuzz

		echo "hogehoge$fizz";
		//結果は　hogehogebuzz

**PHPはダブルコーテーションでくくられた文字列内の変数を展開する**ので

以上のようになる事に注意。


## 数値
数値には小数と整数がある。

	<?php
		$a = 1234;    //整数
		$b = 123.456; //小数

各種計算方法は以下のとおりだ。

小数型と整数型を計算した際の結果の型は、

**実際の計算結果に依存する** が、 **あまりの計算は常に整数となる。**

	<?php
		$a = 5;
		$b = 10.5;
		$c = 8.3;

		//和
		echo $a + $b;

		//差
		echo $b - $a;

		//積
		echo $a * $b;

		//商
		echo $b / $a;

		//あまり
		echo $c % $a;


## 連想配列(配列) 
PHPの配列はすべてkeyとvalueを持った形となる。

keyとvalueの両方を明示的に指定できるが、

指定しない場合は数字となる。

	<?php
		//keyを指定しない形
		$ary = array("fizz","buzz","fizzbuzz");

		//配列全体の確認
		print_r($ary);

		//fizzへのアクセス
		echo $ary[0];

		//keyを指定する形
		$bry = array("a" => A, "b"=>B, "c"=>C);

		//Aへのアクセス
		echo $bry["a"];

		//明示的な指定とそうでない形の混在
		$cry = array("a"=>0,"hogehoge","b"=>1,"fizzbuzz");

		//hogehogeへのアクセス
		//keyを指定しないvalueだけで、カウントされる
		echo $cry[0];

		//fizzbuzzへのアクセス
		echo $cry[1];


また、多次元の配列にすることも可能で

	<?php
		$cry = array(array("a"=>0,"b"=>1,"c"=>2),array("A"=>26, "B"=>27,"C"=>28));

		//0へのアクセスは
		echo $cry[0]["a"];

なども可能。 

## 論理
論理型はtrueとfalseのどちらかである。

日本語的に言うと、真か偽かというところ。

また、論理型を返す演算子は以下のとおりである。
その通りであれば、true、そうでなければfalseを返す。

**左辺と右辺との比較**

	<?php

		$a = 10;
		$b = 15;

		//ゆるい比較演算子
		//等しいかどうか
		echo $a == $b;

		//厳密な比較演算子、基本的にはこちらの利用を推奨。
		//等しいかどうか
		echo $a === $b;

		//等しくないか
		echo $a !== $b;
		echo $a != $b;

		//より大きい、小さい
		echo $a > $b;
		echo $b > $a;

		//以上、以下
		echo $a >= $b;
		echo $a <= $b;

どのような比較をすると、trueかfalseかは[このページ](http://php.net/manual/ja/types.comparisons.php)に非常によくまとまっている。


**複数の比較**

	<?php
		$height = 180;
		$year = 18;
		$work = "学生";

		//~かつ~ならば・・・ 
		if ( 180 < $height AND $height < 200){
			echo "背が高い！";
		}elseif ( $height > 200){
			echo "背がめちゃめちゃ高い！"
		}
		//if(180 < $height && $height < 200)でも同じ。

		//~または~ならば・・・
		if($year < 20 OR $work == "学生" ){
			echo "親の承認が必要です";
		}
		//if($year < 20 || $work == "学生")でも同じ。



##繰り返し
繰り返しを実現するためには以下の方法がある

###for文
	<?php
		//(変数の初期化,条件式,変数の増え方)
		//$i++は下のインクリメントを参考に。
		for($i = 0;$i < 100;$i++){
			//処理
		}
###while文

	<?php
		
		//(条件式)
		while($hoge === true){
			//処理
		}

1回は絶対に実行する場合

	<?php

		do{
			//処理
		}while($hoge === true)
			//(条件式)

###foreach文
配列の長さ分だけ繰り返しを行いたい場合に使う

	<?php

		$ary = array("a"=>"A","b"=>"B","c"=>"C");
		foreach($ary as $key => $value){
			echo $key; //繰り返しの回数番目のkey
			echo $value;	//繰り返しの回数番目のvalue
		}

また、繰り返しと合わせてよく使うものとして、
**インクリメント**(このコードを評価するたびにiに1足す)

	$i++;

**デクリメント**(このコードを評価するたびにiに1引く)

	$i--;

インクリメントやデクリメントは以下の式と同等である。
この手法は四則演算に対して利用できる。

	$i = $i + 1;

##条件分岐
手法は2つ存在し、1つ目はif文、もう一つはswitch文

###if文
もし~なら

	<?php
		$i = 4; 
		//(条件式)
		if($i < 5){
			//処理
		}

もし~なら・・・、そうでないなら***

	<?php
		$i = 11; 
		if($i < 10){
			echo "hello";
		}else{
			echo "good evening"
		}

もし~なら・・・、もし~なら***、そうでないなら+++

	<?php
		$i = 18; 
		if($i < 10){
			echo "hello";
		}elseif( $i < 17){
			echo "good evening"
		}else{
			echo "good night";
		}

###switch文

**breakを記述し忘れるとそれより下の条件も評価します。**

	<?php
		$i = 0;

		switch( $i ){
			//$iが0ならば
			case 0:
				//処理
				break;
			//$iが1ならば
			case 1:
				//処理
				break;
			//$iがどれにも該当しなかったら
			default:
				//処理
				break;
		}


##関数

ある程度同じ処理をする場合、その処理をまとめる手法として。

	<?php
		//関数であることの宣言 関数の名前(引数)
		function doubleEcho($str){
				echo $str;
				echo $str;
		}

		//関数の利用(引数としたいもの)
		doubleEcho("hoge");
	
		//変数を引数ともできる
		$foo = "bar"
		doubleEcho($foo);


**抑えておきたい基礎文法は以上。**